#include "Header.h"
#include "NodeHandler.h"

uint16_t NodeHandler::makeKey(uint8_t x, uint8_t y)
{
	// uint16_t = uint8_t x | uint8_t y
	return (x << 8) | y;
}

NodeHandler::NodeHandler(uint8_t x, uint8_t y)
{
	currentNode = 1;

	Node& startNode = nodes[0];
	startNode.parent = nullptr;
	startNode.x = x;
	startNode.y = y;
	startNode.f = 0;
	nodeMap[makeKey(x, y)] = nodes;

	openNodes[0] = true;
}

void NodeHandler::closeNode(Node * node)
{
	uint32_t id = node - nodes;
	if (id >= MAX_NODES)
	{
		std::cerr << "[Error] Index out of range" << std::endl;
		return;
	}
	openNodes[id] = false;
	closedNodes++;
}

void NodeHandler::openNode(Node * node)
{
	uint32_t id = node - nodes;
	if (id >= MAX_NODES)
	{
		std::cerr << "[Error] Index out of range" << std::endl;
		return;
	}
	if (!openNodes[id])
	{
		openNodes[id] = true;
		closedNodes--;
	}
}

uint16_t NodeHandler::getClosedNodes()
{
	return closedNodes;
}

Node * NodeHandler::createOpenNode(Node * parent, uint8_t x, uint8_t y, uint16_t f)
{
	if (currentNode >= MAX_NODES)
		return nullptr;

	openNodes[currentNode] = true;
	Node* node = nodes + currentNode;
	nodeMap[makeKey(x, y)] = node;
	node->x = x;
	node->y = y;
	node->f = f;
	node->parent = parent;
	currentNode++;
	return node;
}

Node * NodeHandler::getBestNode()
{
	uint16_t best_f = std::numeric_limits<uint16_t>::max();
	int32_t best = -1;

	for (uint16_t i = 0; i < currentNode; i++)
	{
		if (!openNodes[i] || nodes[i].f >= best_f)
			continue;

		best = i;
		best_f = nodes[i].f;
	}

	if (best >= 0)
		return nodes + best;
	return nullptr;
}

std::vector<Position> NodeHandler::getNodePath(const Node& node)
{
	std::vector<Position> path = { Position(node.x, node.y) };

	Node* current = node.parent;
	while (current)
	{
		path.push_back(Position(current->x, current->y));
		current = current->parent;
	}

	return path;
}

Node * NodeHandler::getNodeByPosition(const Position& position)
{
	std::unordered_map<uint16_t, Node*>::const_iterator it = nodeMap.find(makeKey(position.x, position.y));

	if (it == nodeMap.end())
		return nullptr;

	return it->second;
}

Position NodeHandler::getPositionByNode(const Node & node)
{
	return Position(node.x, node.y);
}

std::vector<Position> NodeHandler::getNodeNeighbours(const Node & node)
{
	std::vector<Position> neighbours = {};

	Position range[2] = {
		Position((uint8_t)std::fmax(0, node.x - 1), (uint8_t)std::fmax(0, node.y - 1)),
		Position((uint8_t)std::fmin(GRID_SIZE, node.x + 1), (uint8_t)std::fmin(GRID_SIZE, node.y + 1))
	};

	for (uint8_t y = range[0].y; y <= range[1].y; y++)
	{
		for (uint8_t x = range[0].x; x <= range[1].x; x++)
		{
			if (x == node.x && y == node.y)
				continue;

			neighbours.push_back(Position(x, y));
		}
	}

	return neighbours;
}

uint8_t NodeHandler::getMoveCost(const Node & node, const Position & neighbourPosition)
{
	if (std::abs(node.x - neighbourPosition.x) == std::abs(node.y - neighbourPosition.y))
		return MOVECOST_DIAGONAL;
	return MOVECOST_NORMAL;
}
