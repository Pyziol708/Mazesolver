#ifndef GLOBALS_H
#define GLOBALS_H

#define PROGRAM_NAME "Mazesolver"
#define PROGRAM_VERSION "1.00"
#define PROGRAM_AUTHOR "Kamil Standarski"

#ifdef _DEBUG
#define PROGRAM_COMPILATION "debug"
#else
#define PROGRAM_COMPILATION "public"
#endif // _DEBUG


#define GRID_SIZE 20
#define GRID_FILENAME "grid.txt"
//#define GRID_FILENAME "grid_corrupted.txt"

#define STARTPOINT_X 0
#define STARTPOINT_Y 0
#define ENDPOINT_X 19
#define ENDPOINT_Y 19

#define MOVECOST_NORMAL 10
#define MOVECOST_DIAGONAL 14

#define MAX_NODES 512

enum NODE_TYPE : uint8_t
{
	DEFAULT = 0,
	PATH = 1,
	OBSTACLE = 5
};
#endif