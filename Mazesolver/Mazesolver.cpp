#include "Header.h"
#include "Mazesolver.h"
#include "Pathfinder.h"

void Mazesolver::run()
{
	std::cout << ":: " << PROGRAM_NAME << " " << PROGRAM_VERSION
		<< " (" << PROGRAM_COMPILATION << ")" << std::endl;
	std::cout << ":: by " << PROGRAM_AUTHOR << std::endl;
	std::cout << "::::::::::::::::::::::::::::::::::::::::" << std::endl;

	Grid grid = Grid();
	if (!grid.load())
	{
		system("PAUSE");
		return;
	}
	Pathfinder pathfinder = Pathfinder(&grid);

	std::vector<Position> path = pathfinder.getPath();
	std::vector<Position>::iterator it;
	for (it = path.begin(); it != path.end(); ++it)
	{
		grid.setNodeType(it->x, it->y, NODE_TYPE::PATH);
	}
	grid.display();


	system("PAUSE");
}

int main()
{
	Mazesolver::run();
	return EXIT_SUCCESS;
}