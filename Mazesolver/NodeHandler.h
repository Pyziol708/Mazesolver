#ifndef NODEHANDLER_H
#define NODEHANDLER_H
#include "Position.h"

struct Node
{
	Node* parent;
	uint8_t x, y;
	uint16_t f;
};

class NodeHandler
{
private:
	Node nodes[MAX_NODES] = {};
	bool openNodes[MAX_NODES] = {};
	std::unordered_map<uint16_t, Node*> nodeMap;
	uint16_t currentNode = 0;
	uint16_t closedNodes = 0;

	uint16_t makeKey(uint8_t x, uint8_t y);

public:
	NodeHandler(uint8_t x1 = STARTPOINT_X, uint8_t y1 = STARTPOINT_Y);
	void closeNode(Node* node);
	void openNode(Node* node);
	uint16_t getClosedNodes();
	Node* createOpenNode(Node* parent, uint8_t x, uint8_t y, uint16_t f);
	Node* getBestNode();
	Node* getNodeByPosition(const Position& position);
	std::vector<Position> getNodePath(const Node& node);
	Position getPositionByNode(const Node& node);
	std::vector<Position> getNodeNeighbours(const Node& node);
	static uint8_t getMoveCost(const Node& node, const Position& neighbourPosition);
};

#endif