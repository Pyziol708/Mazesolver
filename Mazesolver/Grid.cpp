#include "Header.h"
#include "Grid.h"

bool Grid::load(std::string filename)
{
	std::ifstream gridFile{ filename };

	if (!gridFile.is_open())
	{
		std::cout << "[Error] Failed to open grid file" << std::endl;
		return false;
	}

	uint8_t buffer[GRID_SIZE][GRID_SIZE];

	uint8_t a;
	for (int i = 0; i < GRID_SIZE; i++)
	{
		for (int j = 0; j < GRID_SIZE; j++)
		{
			a = 0;
			if (!(gridFile >> a) || gridFile.eof())
			{
				std::cout << "[Error] Failed to import grid from grid file!" << std::endl;
				gridFile.close();
				return false;
			}
			buffer[i][j] = (uint8_t)(a - 48);
		}
	}
	gridFile.close();
	for (int i = 0; i < GRID_SIZE; i++)
	{
		for (int j = 0; j < GRID_SIZE; j++)
		{
			this->map[i][j] = buffer[i][j];
		}
	}
	std::cout << "> Grid loaded sucessfully" << std::endl;
	return true;
}

void Grid::display()
{
	std::cout << "> Grid display:" << std::endl;
	for (int i = 0; i < GRID_SIZE; i++)
	{
		for (int j = 0; j < GRID_SIZE; j++)
		{
			std::cout << " " << std::to_string(this->map[i][j]);
		}
		std::cout << std::endl;
	}
}

void Grid::markPath(std::vector<Position> path, NODE_TYPE nodeType)
{
	std::vector<Position>::iterator it;
	for (it = path.begin(); it != path.end(); ++it)
		setNodeType(it->x, it->y, nodeType);
}

void Grid::setNodeType(uint8_t x, uint8_t y, NODE_TYPE nodetype)
{
	if (x >= GRID_SIZE || y >= GRID_SIZE)
		return;
	this->map[y][x] = (uint8_t)nodetype;
}

NODE_TYPE Grid::getNodeType(uint8_t x, uint8_t y)
{
	if (x >= GRID_SIZE || y >= GRID_SIZE)
		return NODE_TYPE::OBSTACLE;
	return (NODE_TYPE)this->map[y][x];
}
