#ifndef PATHFINDER_H
#define PATHFINDER_H

#include "NodeHandler.h"
#include "Grid.h"

class Pathfinder
{
private:
	Grid *grid;
	NodeHandler handler;

public:
	Pathfinder(Grid * grid) : grid(grid) {}
	std::vector<Position> getPath(Position& startPosition = Position(STARTPOINT_X, STARTPOINT_Y),
		Position& endPosition = Position(ENDPOINT_X, ENDPOINT_Y));
	std::vector<Position> getPathNodes(const Node& node);
};

#endif
