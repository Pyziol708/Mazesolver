#ifndef POSITION_H
#define POSITION_H

struct Position
{
	uint8_t x;
	uint8_t y;

	Position() = default;
	Position(uint8_t x, uint8_t y) :x(x), y(y) {}

	bool operator==(const Position& p) const {
		return p.x == x && p.y == y;
	}

	bool operator!=(const Position& p) const {
		return p.x != x || p.y != y;
	}
};

#endif