#include "Header.h"
#include "Pathfinder.h"

std::vector<Position> Pathfinder::getPathNodes(const Node& node)
{
	std::vector<Position> path = { Position(node.x, node.y) };

	Node* current = node.parent;
	while (current)
	{
		path.push_back(Position(current->x, current->y));
		current = current->parent;
	}

	return path;
}

std::vector<Position> Pathfinder::getPath(Position& startPosition, Position& endPosition)
{
	auto t1 = std::chrono::high_resolution_clock::now();
	handler = NodeHandler(startPosition.x, startPosition.y);
	
	Node* current = nullptr;
	while (handler.getClosedNodes() < (GRID_SIZE*GRID_SIZE))
	{
		current = handler.getBestNode();
		if (!current)
			return std::vector<Position>();

		if (handler.getPositionByNode(*current) == endPosition)
		{
			auto t2 = std::chrono::high_resolution_clock::now();
			std::chrono::duration<double, std::milli> fp_ms = t2 - t1;
			std::cout << "> Path found in " << fp_ms.count() << " ms" << std::endl;
			return handler.getNodePath(*current);
		}

		std::vector<Position> neighbours = handler.getNodeNeighbours(*current);
		std::vector<Position>::iterator it;

		Node* neighbourNode;
		for (it = neighbours.begin(); it != neighbours.end(); ++it)
		{
			if (grid->getNodeType(it->x, it->y) != NODE_TYPE::DEFAULT)
				continue;

			neighbourNode = handler.getNodeByPosition(*it);

			uint16_t f = current->f + NodeHandler::getMoveCost(*current, *it);

			if (neighbourNode)
			{
				if (neighbourNode->f <= f)
					continue;

				neighbourNode->parent = current;
				neighbourNode->f = f;
				handler.openNode(neighbourNode);
			}
			else
			{
				neighbourNode = handler.createOpenNode(current, it->x, it->y, f);
				if (!neighbourNode)
					break;
			}
		}
		handler.closeNode(current);
	}

	return std::vector<Position>();
}