#ifndef GRID_H
#define GRID_H

#include "Position.h"

class Grid
{
private:
	uint8_t map[GRID_SIZE][GRID_SIZE] = {};

public:
	Grid() = default;
	bool load(std::string filename = GRID_FILENAME);
	void display();
	void markPath(std::vector<Position> path, NODE_TYPE nodeType = NODE_TYPE::PATH);
	void setNodeType(uint8_t x, uint8_t y, NODE_TYPE nodetype);
	NODE_TYPE getNodeType(uint8_t x, uint8_t y);
};

#endif